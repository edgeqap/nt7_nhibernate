﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using NHibernate;
using NHibernate.Cfg;
using NHibernate.Context;
using NHibernate.Tool.hbm2ddl;

namespace com.divisionq.sample.nhibernate.hbm.utility
{
	/// <summary>
	/// Here basic NHibernate manipulation methods are implemented.
	/// </summary>
	public class NHibernateHelper
	{
		private ISessionFactory _sessionFactory = null;

		/// <summary>
		/// In case there is an already instantiated NHibernate ISessionFactory,
		/// retrieve it, otherwise instantiate it.
		/// </summary>
		public ISessionFactory SessionFactory
		{
			get
			{
				if (_sessionFactory == null)
				{
                    IDictionary<String, String> properties = new Dictionary<String, String>();
                    properties["connection.provider"] = "NHibernate.Connection.DriverConnectionProvider";
                    properties["dialect"] = "NHibernate.Dialect.MySQLDialect";
                    properties["connection.driver_class"] = "NHibernate.Driver.MySqlDataDriver";
                    //properties["connection.connection_string"] = "Server=divq-nvir-prod-db-00.cb47pizdpwev.us-east-1.rds.amazonaws.com;database=edgeqap;user id=administrator;password=33951##(%!d";
                    properties["connection.connection_string"] = "Server=localhost;database=edgeqap;user id=root;password=root";
                    properties["show_sql"] = "true";
                    properties["hbm2ddl.auto"] = "update";

                    NHibernateHelper.log("Propeties");

                    Configuration configuration = new Configuration();
                    configuration.SetProperties(properties);
                    //configuration.AddAssembly("SampleCodes");
                    //                                            C:\\Users\\User\\Desktop\\DivQ.NHibernate\\SampleCodes\\DivQ.NHibernate.MySql.Utility\\hbm.mapping
                    //configuration.AddDirectory(new DirectoryInfo("C:\\Users\\User\\Desktop\\DivQ.NHibernate\\SampleCodes\\DivQ.NHibernate.MySql.Utility\\hbm.mapping"));
                    configuration.AddDirectory(new DirectoryInfo("C:\\Users\\User\\Documents\\bitbucket-scm\\nt7_nhibernate\\nt7_nhibernate\\DivQ.NHibernate.MySql.Utility\\hbm.mapping"));
                    
                   
                    //configuration.AddDirectory(new DirectoryInfo("C:\\Users\\User\\Desktop\\DivQRelease\\DivQ.NHibernate.MySql.Utility\\hbm.mapping"));
                    
                    configuration.SetDefaultAssembly("DivQ.NHibernate.MySql.Utility");

                    NHibernateHelper.log("Added Directory");

					//Configuration configuration = new Configuration();
					//configuration.Configure();

					//new SchemaExport(configuration).Execute(true, true, false);

					// build a Session Factory
					_sessionFactory = configuration.BuildSessionFactory();
                    NHibernateHelper.log("Built config");
				}
				return _sessionFactory;
			}
		}

        public static void log(String message)
        {

            //StreamWriter logger = new StreamWriter("C:\\Users\\User\\Desktop\\log.xml", true);
            var NTPath = "C:\\Users\\User\\Desktop\\DivQ.NHibernate\\";
            StreamWriter logger = new StreamWriter(NTPath + DateTime.Now.ToString("yyyyMMdd") + ".xml", true);
            logger.WriteLine(DateTime.Now.ToString("yyyyMMdd HH:mm:ss") + " :: " + message);
            //logger.Write("<div>" + message.Replace("<", "&lt;").Replace(">", "&gt;") + "</div>");


            logger.Close();
        }

		/// <summary>
		/// Open an ISession based on the built SessionFactory.
		/// </summary>
		/// <returns>Opened ISession.</returns>
		public ISession OpenSession()
		{
			return SessionFactory.OpenSession();

		}
		/// <summary>
		/// Create an ISession and bind it to the current tNHibernate Context.
		/// </summary>
		public void CreateSession()
		{
			CurrentSessionContext.Bind(OpenSession());
		}

		/// <summary>
		/// Close an ISession and unbind it from the current
		/// NHibernate Context.
		/// </summary>
		public void CloseSession()
		{
			if (CurrentSessionContext.HasBind(SessionFactory))
			{
				CurrentSessionContext.Unbind(SessionFactory).Dispose();
			}
		}

		/// <summary>
		/// Retrieve the current binded NHibernate ISession, in case there
		/// is any. Otherwise, open a new ISession.
		/// </summary>
		/// <returns>The current binded NHibernate ISession.</returns>
		public ISession GetCurrentSession()
		{
			if (!CurrentSessionContext.HasBind(SessionFactory))
			{
				CurrentSessionContext.Bind(SessionFactory.OpenSession());
			}
			return SessionFactory.GetCurrentSession();
		}
	}

	/// <summary>
	/// Helper methods for dealing with NHibernate ISession.
	/// </summary>
	public class SessionHelper
	{
		/// <summary>
		/// NHibernate Helper
		/// </summary>
		private NHibernateHelper _nHibernateHelper = null;

		public SessionHelper()
		{
			_nHibernateHelper = new NHibernateHelper();
		}

		/// <summary>
		/// Retrieve the current ISession.
		/// </summary>
		public ISession Current
		{
			get
			{
				return _nHibernateHelper.GetCurrentSession();
			}
		}

		/// <summary>
		/// Create an ISession.
		/// </summary>
		public void CreateSession()
		{
			_nHibernateHelper.CreateSession();
		}

		/// <summary>
		/// Clear an ISession.
		/// </summary>
		public void ClearSession()
		{
			Current.Clear();
		}

		/// <summary>
		/// Open an ISession.
		/// </summary>
		public void OpenSession()
		{
			_nHibernateHelper.OpenSession();
		}

		/// <summary>
		/// Close an ISession.
		/// </summary>
		public void CloseSession()
		{
			_nHibernateHelper.CloseSession();
		}
	}
}