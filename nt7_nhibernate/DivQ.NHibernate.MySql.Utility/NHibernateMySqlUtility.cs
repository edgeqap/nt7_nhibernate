﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using com.divisionq.sample.nhibernate.hbm.classmap;
using com.divisionq.sample.nhibernate.hbm.utility.dao;
using com.divisionq.sample.nhibernate.hbm.utility;

namespace DivQ.NHibernate.MySql.Utility
{
    public class NHibernateMySqlUtility
    {
        public static String TestSimplePlainObject()
        {
            Employee emp = new Employee();
            emp.FirstName = "Test - ";
            emp.LastName = (DateTime.Now).ToLongTimeString();

            return emp.FirstName + emp.LastName;
        }

        public static String TestDAO()
        {
            Employee emp = new Employee();
            emp.FirstName = "Test Employee - ";
            emp.LastName = (DateTime.Now).ToLongTimeString();

            NHibernateGenericDAO<Employee> dao = new NHibernateGenericDAO<Employee>();
            dao.Add(emp);
            dao = null;

            return "TestDAO - Complete";
        }
    }
}
