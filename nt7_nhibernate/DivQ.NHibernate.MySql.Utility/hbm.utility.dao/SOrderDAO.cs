﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;

using NHibernate;

using com.divisionq.sample.nhibernate.hbm.classmap;

namespace com.divisionq.sample.nhibernate.hbm.utility.dao
{
    public class SOrderDAO<T> : IBaseDAO<T> where T : SOrder
    {
        NHibernateHelper _helper = new NHibernateHelper();
        ISession session = null;

        public void Add(T obj)
        {
            if (session == null)
                session = _helper.OpenSession();
            
            ITransaction transaction = session.BeginTransaction();
            session.Save(obj);
            transaction.Commit();
        }

        public void Update(T obj)
        //public void Update(T obj).where T : class, ISearchableEntity 
        {
            //throw new NotImplementedException();
            
            if (session == null)
                session = _helper.OpenSession();

            ITransaction transaction = session.BeginTransaction();
            session.Update(obj);
            //session.Merge<T>(obj);
            //session.Merge((SOrder)obj );
            transaction.Commit();
        }

        public void Merge(T obj) 
        {
            if (session == null)
                session = _helper.OpenSession();

            ITransaction transaction = session.BeginTransaction();
            T retObj = session.Merge<T>(obj);
            transaction.Commit();
        }

        public void Remove(T obj)
        {
            throw new NotImplementedException();
        }

        public T getObjectByUid(long uid)
        {
            if (session == null)
                session = _helper.OpenSession();

            return session.Load<T>(uid);
        }


        public T getObjectByToken(String fieldName, String fieldValue)
        {
            if (session == null)
                session = _helper.OpenSession();

            var orders = session.CreateQuery("from " + typeof(T).Name + " b where b." + fieldName + " = :" + fieldName + "")
                                    //  session  .CreateCriteria<SOrder>()
                                    //.Add(Restrictions.Eq(fieldName, fieldValue))
                                    .SetParameter( fieldName, fieldValue)
                                    .List<T>();
    
            if (orders.Count > 0)
                return (T)orders[0];
            else
                return null;
        }

        public T getObjectByFields(IDictionary<String, String> parameters)
        {
            if (session == null)
                session = _helper.OpenSession();
    
            var myQuery = "from " + typeof(T).Name + " b where"; // b." + fieldName + " = :" + fieldName + "";

            int index = 0;
            foreach (String fieldName in parameters.Keys)
            {
                if ( index != 0 )
                    myQuery += " AND ";

                myQuery += " b." + fieldName + " = :" + fieldName + " ";

                index++;
            }
            IQuery query = session.CreateQuery(myQuery);
            
            foreach ( String fieldName in parameters.Keys )
            {
                String fieldValue = parameters[ fieldName ];
                query = query.SetParameter(fieldName, fieldValue);
            }

            var orders = query.List<T>();

            if (orders.Count > 0)
                return (T)orders[0];
            else
                return null;
        }
    }
    

    
}
