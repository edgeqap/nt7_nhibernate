﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.divisionq.sample.nhibernate.hbm.utility.dao
{
	public interface IBaseDAO<T>
	{
		void Add(T obj);
		void Update(T obj);
		void Remove(T obj);

		T getObjectByUid(Int64 uid);
	}
}
