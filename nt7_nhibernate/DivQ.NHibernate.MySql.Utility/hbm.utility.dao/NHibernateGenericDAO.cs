﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NHibernate;

namespace com.divisionq.sample.nhibernate.hbm.utility.dao
{
	public class NHibernateGenericDAO<T> : IBaseDAO<T>
	{
		NHibernateHelper _helper = new NHibernateHelper();
        ISession session = null;

		public void Add(T obj)
		{
            if(session == null)
			    session = _helper.OpenSession();

			ITransaction transaction = session.BeginTransaction();
			session.Save(obj);
			transaction.Commit();
		}

		public void Update(T obj)
		{
            if (session == null)
                session = _helper.OpenSession();

            ITransaction transaction = session.BeginTransaction();
            session.Update(obj);
            transaction.Commit();
			//throw new NotImplementedException();
		}

		public void Remove(T obj)
		{
            if (session == null)
                session = _helper.OpenSession();

            ITransaction transaction = session.BeginTransaction();
            session.Delete(obj);
            transaction.Commit();
			//throw new NotImplementedException();
		}

		public T getObjectByUid(long uid)
		{
            if (session == null)
                session = _helper.OpenSession();

            return session.Load<T>(uid);
			//throw new NotImplementedException();
		}
	}
}
