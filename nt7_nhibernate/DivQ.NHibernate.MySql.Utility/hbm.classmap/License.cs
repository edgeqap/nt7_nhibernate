﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.divisionq.sample.nhibernate.hbm.classmap
{
    public class License
    {
        long    id;

        string  marketID;
        bool    hasLicense;
        string  riskLevel;
        string  remarks;

        DateTime time;


        public long Id
        {
            get { return id;    }
            set { id = value;   }
        }

        public string MarketID
        {
            get { return marketID;  }
            set { marketID = value; }
        }

        public bool HasLicense
        {
            get { return hasLicense;        }
            set { this.hasLicense = value;  }
        }

        public string RiskLevel
        {
            get { return riskLevel;     }
            set { riskLevel = value;    }
        }

        public string Remarks
        {
            get { return remarks;   }
            set { remarks = value;  }
        }

        public DateTime Time
        {
            get { return time;  }
            set { time = value; }
        }

    }
}
