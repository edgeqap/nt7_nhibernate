﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.divisionq.sample.nhibernate.hbm.classmap
{
	public class Person : BaseEntity
	{
		private String firstName;
		private String lastName;
		private DateTime dateOfBirth;
		private String identifier;

		public virtual String Identifier
		{
			get { return identifier; }
			set { identifier = value; }
		}

		public virtual DateTime DateOfBirth
		{
			get { return dateOfBirth; }
			set { dateOfBirth = value; }
		}

		public virtual String LastName
		{
			get { return lastName; }
			set { lastName = value; }
		}

		public virtual String FirstName
		{
			get { return firstName; }
			set { firstName = value; }
		}
		
	}
}
