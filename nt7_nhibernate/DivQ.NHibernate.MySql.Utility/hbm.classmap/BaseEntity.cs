﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.divisionq.sample.nhibernate.hbm.classmap
{
    public class BaseEntity
    {
        private long id = -1;

        public virtual long Id
        {
            get { return id; }
            set { id = value; }
        }
    }
}
