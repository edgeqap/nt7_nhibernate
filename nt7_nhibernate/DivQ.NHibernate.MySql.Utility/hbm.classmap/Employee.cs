﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace com.divisionq.sample.nhibernate.hbm.classmap
{
	public class Employee : Person
	{
		private String appointment;
		private String position;
		private Department department;
		private DateTime dateOfEmployment;
		private IList<Incentive> incentives = new List<Incentive>();

		public virtual IList<Incentive> Incentives
		{
			get { return incentives; }
			set { incentives = value; }
		}


		public virtual DateTime DateOfEmployment
		{
			get { return dateOfEmployment; }
			set { dateOfEmployment = value; }
		}


		public virtual Department Department
		{
			get { return department; }
			set { department = value; }
		}


		public virtual String Position
		{
			get { return position; }
			set { position = value; }
		}

		public virtual String Appointment
		{
			get { return appointment; }
			set { appointment = value; }
		}

        public override String ToString()
        {
            return "id: " + base.Id + ", firstname: " + base.FirstName + ", lastname: " + base.LastName;
        }
	}
}
