﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.divisionq.sample.nhibernate.hbm.classmap
{
	public enum IncentiveType
	{
		BasicSalary,
		Bonus,
		Allowance,
		Reimbursement
	}

	public class Incentive : BaseEntity
	{
		private IncentiveType type;
		private double amount;
		private DateTime dateOfProcess;
		private Employee employee;

		public virtual Employee Employee
		{
			get { return employee; }
			set { employee = value; }
		}


		public virtual DateTime DateOfProcess
		{
			get { return dateOfProcess; }
			set { dateOfProcess = value; }
		}
		

		public virtual double Amount
		{
			get { return amount; }
			set { amount = value; }
		}


		public virtual IncentiveType Type
		{
			get { return type; }
			set { type = value; }
		}

	}
}
