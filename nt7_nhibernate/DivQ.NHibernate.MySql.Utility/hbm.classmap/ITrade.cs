﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using NinjaTrader.Cbi;

namespace com.divisionq.sample.nhibernate.hbm.classmap
{
    public class ITrade
    {
        private bool isLong = true;
        private bool isSim = true;
        private IOrder entryOrder;
        private IOrder exitOrder;
        private IOrder stopOrder;

        public ITrade(){}

        public ITrade(bool isLong, bool isSim)
        {
            setIsLong(isLong);
            setIsSim(isSim);
        }

        public bool getIsLong()
        {
            return this.isLong;
        }

        private void setIsLong(bool isLong)
        {
            this.isLong = isLong;
        }

        public bool getIsSim()
        {
            return this.isSim;
        }

        private void setIsSim(bool isSim)
        {
            this.isSim = isSim;
        }

        public IOrder getEntryOrder()
        {
            return this.entryOrder;
        }

        public void setEntryOrder(IOrder entryOrder)
        {
            this.entryOrder = entryOrder;
        }

        public IOrder getExitOrder()
        {
            return this.exitOrder;
        }

        public void setExitOrder(IOrder exitOrder)
        {
            this.exitOrder = exitOrder;
        }

        public IOrder getStopOrder()
        {
            return this.stopOrder;
        }

        public void setStopOrder(IOrder stopOrder)
        {
            this.stopOrder = stopOrder;
        }

    }
}
