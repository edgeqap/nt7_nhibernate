﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NinjaTrader.Cbi;
using NinjaTrader.Data;

namespace com.divisionq.sample.nhibernate.hbm.classmap
{
    public class SOrder:IOrder
    {
		double avgFillPrice = 0;
		DateTime time;

        long id;
        

        // Self Included Fields for Alan's personnel Operations
        bool isSim;
        string accountID;
        string backtestRun;
        string strategyName;

        // Self Added
        int filled;
        double limitPrice;
        bool liveUntilCancelled;
        string nativeError;
        string oco;
        string orderId;
        bool overFill;
        double stopPrice;
        string fromEntrySignal;
        //ErrorCode errorCode;


        public long Id
        {
            get { return id; }
            set { id = value; }
        }


        public bool IsSim
        {
            //get { throw new NotImplementedException(); }
            get { return isSim; }
            set { isSim = value; }
        }

        public string AccountID
        {
            //get { throw new NotImplementedException(); }
            get { return accountID; }
            set { accountID = value; }
        }

        public string StrategyName
        {
            //get { throw new NotImplementedException(); }
            get { return strategyName; }
            set { strategyName = value; }
        }

        public string BacktestRun
        {
            //get { throw new NotImplementedException(); }
            get { return backtestRun; }
            set { backtestRun = value; }
        }




        public DateTime Time
        {
            //get { throw new NotImplementedException(); }
            get { return time; }
            set { time = value; }
        }
		
		// Preciision to the Min Tick Size
        public double AvgFillPrice
        {
            //get { throw new NotImplementedException(); }
			get { return avgFillPrice; }
			set { avgFillPrice = value; }
        }


        string errorCodeString;
        public ErrorCode Error
        {
            //get { throw new NotImplementedException(); }
            get { return (ErrorCode)Enum.Parse(typeof(ErrorCode), errorCodeString);   }
            set { errorCodeString = value.ToString(); }
        }

        public string ErrorStr
        {
            get { return this.errorCodeString; }
            set { this.errorCodeString = value; }
        }

        public int Filled
        {
            //get { throw new NotImplementedException(); }
            get { return filled; }
            set { filled = value; }
        }

        public string FromEntrySignal
        {
            //get { throw new NotImplementedException(); }
            get { return fromEntrySignal; }
			set { this.fromEntrySignal = value; }
        }

        // This is not mapped
        public Instrument Instrument
        {
            get { throw new NotImplementedException(); }
        }

        public double LimitPrice
        {
            //get { throw new NotImplementedException(); }
            get { return limitPrice; }
            set { this.limitPrice = value; }
        }

        public bool LiveUntilCancelled
        {
            //get { throw new NotImplementedException(); }
            get { return liveUntilCancelled; }
            set { this.liveUntilCancelled = value; }
        }

		private string name;
        public string Name
        {
           // get { throw new NotImplementedException(); }
			get { return name; }
			set { name = value; }
        }

		private string token;
        public string Token
        {
           // get { throw new NotImplementedException(); }
			get { return token; }
			set { token = value; }
        }
		
        public string NativeError
        {
            //get { throw new NotImplementedException(); }
            get { return nativeError; }
            set { this.nativeError = value; }
        }

        public string Oco
        {
            //get { throw new NotImplementedException(); }
            get { return oco; }
            set { this.oco = value; }
        }

        //private OrderAction orderAction;
        private string orderActionStr = "";
        public OrderAction OrderAction
        {
            //get { throw new NotImplementedException(); }
			//get { return orderAction; }
			//set { orderAction = value; }
            get { return (OrderAction)Enum.Parse(typeof(OrderAction), orderActionStr); }
            set { orderActionStr = value.ToString(); }
        }

        public string OrderActionStr
        {
            get { return this.orderActionStr; }
            set { this.orderActionStr = value; }
        }

        public string OrderId
        {
            //get { throw new NotImplementedException(); }
            get { return orderId; }
            set { this.orderId = value; }
        }

        /*
		private OrderState orderState = OrderState.Accepted;
        public OrderState OrderState
        {
            //get { throw new NotImplementedException(); }
            //get { return orderState; }
            //set { this.orderState = value; }
            get { return this.orderState; }
            set { this.orderState = value; }
        }

        public string OrderStateStr
        {
            get { return this.orderState.ToString(); }
            set { this.orderState = OrderState.Accepted; }
        }
        */

        
        private string orderStateStr;
        public OrderState OrderState
        {
            //get { throw new NotImplementedException(); }
			//get { return orderState; }
            //set { this.orderState = value; }
            get { return (OrderState)Enum.Parse(typeof(OrderState), orderStateStr); }
            set { orderStateStr = value.ToString(); }
        }

        public string OrderStateStr
        {
            get { return this.orderStateStr; }
            set { this.orderStateStr = value; }
        }
        

        //private OrderType orderType;
        private string orderTypeString;
        public OrderType OrderType
        {
            //get { throw new NotImplementedException(); }
			//get { return orderType; }
            //set { this.orderType = value; }
            get { return (OrderType)Enum.Parse(typeof(OrderType), orderTypeString); }
            set { orderTypeString = value.ToString(); }
        }

        public string OrderTypeStr
        {
            //get { throw new NotImplementedException(); }
            get { return orderTypeString; }
            set { this.orderTypeString = value; }
        }

        public bool OverFill
        {
            //get { throw new NotImplementedException(); }
            get { return overFill; }
            set { this.overFill = value; }
        }

		int quantity;
        public int Quantity
        {
            //get { throw new NotImplementedException(); }
			get { return quantity; }
			set { quantity = value; }
        }

        public double StopPrice
        {
            //get { throw new NotImplementedException(); }
            get { return stopPrice; }
            set { this.stopPrice = value; }
        }

		/*
        public DateTime Time
        {
            get { throw new NotImplementedException(); }
        }
		*/
        //private OrderType orderType;
        private string timeInForceString;
        public TimeInForce TimeInForce
        {
            //get { throw new NotImplementedException(); }
            //set { this.timeInForce = value; }
            get { return (TimeInForce)Enum.Parse(typeof(TimeInForce), timeInForceString); }
            set { timeInForceString = value.ToString(); }
        }

        public string TimeInForceStr
        {
            //get { throw new NotImplementedException(); }
            get { return timeInForceString; }
            set { this.timeInForceString = value; }
        }


    }
}
