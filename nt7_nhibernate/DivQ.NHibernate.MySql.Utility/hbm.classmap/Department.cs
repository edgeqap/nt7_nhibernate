﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.divisionq.sample.nhibernate.hbm.classmap
{
	public class Department : BaseEntity
	{
		private String name;
		private IList<Employee> staffs = new List<Employee>();
		private Employee departmentHead;

		public virtual Employee DepartmentHead
		{
			get { return departmentHead; }
			set { departmentHead = value; }
		}


		public virtual IList<Employee> Staffs
		{
			get { return staffs; }
			set { staffs = value; }
		}
		

		public virtual String Name
		{
			get { return name; }
			set { name = value; }
		}

	}
}
