﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using com.divisionq.sample.nhibernate.hbm.classmap;
using com.divisionq.sample.nhibernate.hbm.utility.dao;

using NinjaTrader.Cbi;

namespace ModuleTester
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void TestButton_Click(object sender, EventArgs e)
        {
            //ITrade trade = new ITrade(true, true);
            //SOrder order = new SOrder();

            IBaseDAO<SOrder> OrderDAO = new SOrderDAO<SOrder>();

            SOrder order = new SOrder();
            order.Name = "TestEntry1";
            //Console.WriteLine("ErrorCode.OrderRejected.ToString(): " + ErrorCode.OrderRejected);
            //ErrorCode myStuff = ErrorCode.UnableToChangeOrder;
            //ErrorCode myStuff = new ErrorCode(); // ErrorCode.UnableToChangeOrder;
            //myStuff = NinjaTrader.Cbi.ErrorCode.UnableToChangeOrder;
            //myStuff.
            //order.ErrorStr = myStuff + "";

            order.ErrorStr = "OrderRejected"; //ErrorCode.OrderRejected.ToString();
            order.OrderActionStr = "Buy"; // OrderAction.Buy.ToString(); // "TestString"; //ErrorCode.OrderRejected.ToString();
            //order.OrderAction = OrderAction.Buy;
            //order.OrderState = OrderState.Accepted;
            
            //order.ErrorStr = ErrorCode.OrderRejected.ToString();

            // dept.Name = "AsiaPacBusiness";
            // order.
            
            Console.WriteLine("Add SOrder");
            OrderDAO.Add(order);


            Console.WriteLine("Added SOrder");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SOrderDAO<SOrder> OrderDAO = new SOrderDAO<SOrder>();


            // SOrder order = OrderDAO.getObjectByUid( 1 );
            // SOrder order = OrderDAO.getSorderByUid(1);
            
            SOrder order = OrderDAO.getObjectByUid( 1 );
            Console.WriteLine("Add SOrder");
            
            //SOrder order = null;
            Console.WriteLine("SOrder UID : " + order.Id);
            Console.WriteLine("SOrder Name: " + order.Name);
            
            Console.WriteLine("Added SOrder");
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            NHibernateGenericDAO<Employee> dao = new NHibernateGenericDAO<Employee>();
            Employee emp = dao.getObjectByUid(3);
            Console.WriteLine(emp.ToString());
        }

        private void button4_Click(object sender, EventArgs e)
        {
            NHibernateGenericDAO<Employee> dao = new NHibernateGenericDAO<Employee>();
            Employee emp = dao.getObjectByUid(3);
            Console.WriteLine(emp.ToString());
            emp.FirstName = "John";
            emp.LastName = "Hancock";
            Console.WriteLine(emp.ToString());
            dao.Update(emp);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            NHibernateGenericDAO<Employee> dao = new NHibernateGenericDAO<Employee>();
            Employee emp = dao.getObjectByUid(3);
            Console.WriteLine(emp.ToString());
            dao.Remove(emp);
            //emp = dao.getObjectByUid(3);
            //Console.WriteLine(emp.ToString());
        }

    }
}
